# apt-mirror

## Quick Start

```bash
cp .env.example .env
# edit .env
docker-compose up -d
docker-compose exec mirror mirror
```

Whenever you want to update the mirror, repeat:

```bash
docker-compose exec mirror mirror
```

To use the mirror as a package source from other hosts, replace all the occurrence of
the source address in `/etc/apt/sources.list` with the value of `MIRROR_DOMAIN` in `.env` in the hosts.

For example, if the package source address in `/etc/apt/sources.list` is `kr.archive.ubuntu.com`
and the value of `MIRROR_DOMAIN` in `.env` is `mirror.example`, 
then replace all `kr.archive.ubuntu.com` in `/etc/apt/sources.list` with `mirror.example`:

```bash
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
sudo sed -i "s|kr.archive.ubuntu.com|mirror.example|" /etc/apt/sources.list
sudo apt-get update
```

## References

* [https://www.linuxtechi.com/setup-local-apt-repository-server-ubuntu/](https://www.linuxtechi.com/setup-local-apt-repository-server-ubuntu/)
* [https://blog.programster.org/set-up-a-local-ubuntu-mirror-with-apt-mirror](https://blog.programster.org/set-up-a-local-ubuntu-mirror-with-apt-mirror)
